# java 基础知识

## Effctive java 笔记

1. 用静态工厂方法替代公有构造器
	* 方法有名称，更容易看得懂代码
	* 不必每次调用都创建一个新的对象
    * 可以返回原返回类型的任何子类型,灵活
2. 多个构造器参数时，考虑构建器
	* 多个构造参数时，javaBean 模式可以解决，但是有缺陷；构造过程被分到几个调用中，构造过程中，可能出现不一致的情况
3. 用私有化构造器或者枚举类型强化singleton 属性
4. 通过私有化构造器强化不可实例的能力
5. 避免创建不必要的对象
	* 能用基本类型用基本类型，当心自动装箱
	* String a = new String("abc"); 不如
	* String a = "abc"; 好

6. 消除过期对象引用
	* 只要是类自己管理内存，程序员就要注意内存泄漏问题
	* 内存泄漏的另一个重要来源，就是缓存；
		WeakHashMap，Entry 是 弱引用，当 缓存项的生命周期由指向该缓存项的外部引用决定，而不是缓存项值本身决定的时候，才可以用WeakHashMap
	* 内存泄漏的另一个常见就是 监听器和回调
7. 优先考虑依赖注入引入资源
8. 避免使用finalizer 或者cleaner 来做gc
9. try_with_resource 优于try_finally
10. 重写Object equals 方法时需要遵守的约定
	> 大部分情况下，Object 自带的equal方法（同一个对象实例 才相等）满足场景
	>> 一些超类覆盖了equal方法，满足子类需求；比如Set 从 AbstractSet 即成 equal实现；List 从 AbstractList 
    即成 equal实现；Map 从 AbstractMap 即成 equal实现；
	>>> 对于一些必须 要实现逻辑比对的“值类”，需要满足 一下几个特性
	>>>* 对称性 对于任何非null的引用值 x.equals(y) 成立，那么 y.equals(x) 也必须成立
		传递性 对于任何非null的引用值 x.equals(y) 并且  y.equals(z) ,那么 x.equals(z) 一定成立
	>>> * 一致性 对于任何非null的引用值 只要equals 方法里所用的对象里的信息没有被修改，那么 x.equals(y) 应该一致成立
	>>> * 对于任何非null的引用值 x.equal(null) 必须返回 false
	代替手工测试equal 和 hashcode 方法的框架 google的autoValue 
11. 覆盖equals 时必须也覆盖 hashcode
	> int prime = 31;
	int result = Short.hashcode(field1)
	result = prime * result + Short.hashcode(field2)
	result = prime * result + Short.hashcode(field3)
	return result;
12. 始终要覆盖toString()
13. 谨慎的重写 clone方法
14. 考虑实现Comparable 接口

	* comparable 的CompareTo 方法也可以使用Comparator比较构造器， 推荐使用 Comparator比较构造器
	* Comparator比较构造器 返回 Comparator比较器
	装箱类的compare 静态方法，避免使用 < >
15. 使类和成员的访问最小化
16. 要在公有类而非公有方法中使用访问方法
17. 是可变性最小化
	>  原来Integer 的常量池缓存 是-128 到 一个可配置到 high value。缓存的 int[] cache array size 最大是 Integer.MAX_VALUE。如果没有配置或者配置的值 小于127，那么就按照127 算。即Integer的常量池缓存，至少是 -128 到127，最大可缓存 -128 到 Integer.MAX_VALUE - 128 -1.

