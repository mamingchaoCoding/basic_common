package com.mamingchao.basic.access_;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 对于public 的final 数组，优化为更安全的方式
 *
 * Created by mamingchao on 2020/12/29.
 */
public class SecurityFinalArray {
    // 这种是非线程安全的，虽然 数组的引用不变，但是数组的内容可变
    public final static String[] VALUES = new String[10];

    //方式一  公有的原始数组改成私有的，然后 暴露出 只读的原数组
    private final static String[] PRIVATE_VALUE = new String[20];
    public final static List<String> ARRAY = Collections.unmodifiableList(Arrays.asList(PRIVATE_VALUE));

    //方式二 公有的原始数组改成私有的，然后 暴露出 原数组的副本

    private final static String[] PRIVATE_VALUE1 = new String[20];
    public final static String[] ARRAY1 = PRIVATE_VALUE1.clone();
}
