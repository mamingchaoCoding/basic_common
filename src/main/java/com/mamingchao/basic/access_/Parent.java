package com.mamingchao.basic.access_;

/**
 * Created by mamingchao on 2020/12/28.
 */
public class Parent {

    String name;// default
    protected int age; // protected
    private boolean gender; // private

    class PackagePrivate{
        String defaultName;// default
        protected int defaultAge; // protected
        private boolean defaultGender; // private

        PackagePrivate(){}

        @Override
        public String toString() {
            return "packagePrivate{" +
                    "defaultName='" + defaultName + '\'' +
                    ", defaultAge=" + defaultAge +
                    ", defaultGender=" + defaultGender +
                    '}';
        }
    }

    private class PrivateClass{
        String privateName;// default
        protected int privateAge; // protected
        private boolean privateGender; // private

        @Override
        public String toString() {
            return "privateClass{" +
                    "privateName='" + privateName + '\'' +
                    ", privateAge=" + privateAge +
                    ", privateGender=" + privateGender +
                    '}';
        }
    }

    protected class ProtectedClass {
        String protectedName;// default
        protected int protectedAge; // protected
        private boolean protectedGender; // private

        @Override
        public String toString() {
            return "protectedClass{" +
                    "protectedName='" + protectedName + '\'' +
                    ", protectedAge=" + protectedAge +
                    ", protectedGender=" + protectedGender +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Parent{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }
}
