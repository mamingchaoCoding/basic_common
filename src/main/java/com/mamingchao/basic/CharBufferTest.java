package com.mamingchao.basic;

import java.nio.CharBuffer;

/**
 * Created by mamingchao on 2021/1/23.
 */
public class CharBufferTest{

    public static void main(String[] args) {
        CharBuffer buff = CharBuffer.allocate(8);
        System.out.println("capacity: " + buff.capacity());
        System.out.println("limit: " + buff.limit());;
        System.out.println("position:" + buff.position());

        System.out.println("-------------1--------------");

        buff.put('a');
        buff.put('b');
        buff.put('c');
        System.out.println("position:" + buff.position());

        System.out.println("------------2---------------");

        buff.flip();
        System.out.println("limit: " + buff.limit());;
        System.out.println("position:" + buff.position());

        System.out.println("-------------3--------------");

        System.out.println(buff.get());
        System.out.println(buff.position());

        System.out.println("-------------4--------------");

        buff.clear();
        System.out.println("limit: " + buff.limit());;
        System.out.println("position:" + buff.position());

        System.out.println("-------------5--------------");

        System.out.println(buff.get());
        System.out.println(buff.get(1));
        System.out.println(buff.get(2));
        System.out.println("position:" + buff.position());
    }
}
