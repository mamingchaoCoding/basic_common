package com.mamingchao.basic.rmi;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by mamingchao on 2021/1/23.
 */
public class ServiceProvider {
    public static void main(String[] args) {

        SayService sayService = new SayServiceImpl();

        try {
            UnicastRemoteObject.exportObject(sayService,55533,new ClientSocketFactory(),new ProviderSocketFactory());

            Registry registry = LocateRegistry.createRegistry(999);
            registry.bind("sayService",sayService);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
        }
    }
}
