package com.mamingchao.basic.rmi;

import java.rmi.Remote;

/**
 * Created by mamingchao on 2021/1/23.
 */
public class SayServiceImpl  implements SayService {
    @Override
    public String say(String message) throws Exception{
        return "I am say " + message;
    }
}
