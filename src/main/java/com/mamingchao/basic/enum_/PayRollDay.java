package com.mamingchao.basic.enum_;

/**
 * Created by mamingchao on 2021/1/12.
 */
public enum PayRollDay {

    MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY(DayType.WEEKEND),SUNDAY(DayType.WEEKEND);

    private DayType dayType;

    private PayRollDay(DayType dayType){
        this.dayType = dayType;
    }

    private PayRollDay(){
        this(DayType.WEEKDAY);
    }

    double pay( double payRate, int workedMunites){
        return dayType.caculateOvertimeSalary(payRate, workedMunites);
    }


    private enum DayType{

        WEEKEND{double caculateOvertimeSalary(double salaryPerMunite, int workingMunite){
            return (salaryPerMunite * workingMunite)/2;
        }},
        WEEKDAY{double caculateOvertimeSalary(double salaryPerMunite, int workingMunite){
            return workingMunite - MIN_DAY_SHIFT > 0 ? (workingMunite - MIN_DAY_SHIFT) * salaryPerMunite : 0;
        }};

        private final static int MIN_DAY_SHIFT = 8 * 60;
        abstract double caculateOvertimeSalary(double salaryPerMunite, int workingMunite);
    }
}
