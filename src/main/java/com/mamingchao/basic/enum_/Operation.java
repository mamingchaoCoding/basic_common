package com.mamingchao.basic.enum_;

/**
 * Created by mamingchao on 2021/1/12.
 */
public enum  Operation {

    ADD("+"){public int apply(int x, int y){return x + y;}},
    MINUS("-"){public int apply(int x, int y){return x - y;}},
    TIMES("x"){public int apply(int x, int y){return x * y;}},
    DIVIDE("/"){public int apply(int x, int y){return x / y;}};

    private String symbol;
    private Operation(String symbol){
        this.symbol = symbol;
    }
    public abstract int apply(int x, int y);

    @Override
    public String toString(){return symbol;}

    public static void main(String[] args){
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);

        for(Operation op : Operation.values()){
            System.out.printf("%d  %s  %d = %d %n",x,op,y,op.apply(x,y));
        }
    }
}
