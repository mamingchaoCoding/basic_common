package com.mamingchao.basic.cookie;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * http 是无状态的，每次请求 得到返回了，连接就断了
 * 为了实现 http 的有状态，那么就有了session技术。cookie是 session技术的一个组成部分，客户端侧保存 http状态信息
 * cookie 是客户端某一次访问服务端的时候，由 服务器端 传给 客户端的
 * 浏览器保存了cookie，那么在以后的每次访问的请求头里，就会带有cookie 里保存的信息
 * 一个web站点可以给浏览器发送多个cookie，一个浏览器也可以存多个web 站点的cookie

 * Created by mamingchao on 2021/1/14.
 */
public class MyServlet extends HttpServlet{

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("GBK");
        Cookie cookie = new Cookie("token","100010");
        //cookie的有效期，单位是秒
        cookie.setMaxAge(3 * 24 * 3600);
        //cookie 绑定的请求url,只有访问ip:port/cookie/abc的时候，该cookie才生效
        cookie.setPath("/cookie/abc");
        resp.getWriter().write("cookie test");

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }
}
