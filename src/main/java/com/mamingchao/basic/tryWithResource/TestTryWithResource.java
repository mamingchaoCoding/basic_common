package com.mamingchao.basic.tryWithResource;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * try_with_resource 必须实现了 AutoCloseable (Closeable 是其子接口)
 * 如果方法体和 finally 里的逻辑 都 抛异常，try_finally stack error 最后一条错误信息会把前面 的错误信息 覆盖掉
 * 而 try_with_resource 会打印第一条错误信息，并且告诉你后面的错误因为 发生里异常而被 禁止打印
 *
 * 所以，在处理必须关闭的资源时（实现里Closeable 或者 AutoCloseable接口的资源） 用try_with_resource 更合适
 *
 * 因为 fia
 * Created by mamingchao on 2020/12/22.
 */
public class TestTryWithResource {

    public static void main(String[] args) throws Exception{
        String filePath = "";
        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            br.readLine();
        }finally {
            System.out.println("finally is called");
        }


//        BufferedReader br = null;
//        try {
//            br = new BufferedReader(new FileReader(filePath));
//            br.read();
//        } finally {
//            br.close();
//        }
    }

    public static void testMethod(){
        System.out.println("testMethod is called");
    }
}
