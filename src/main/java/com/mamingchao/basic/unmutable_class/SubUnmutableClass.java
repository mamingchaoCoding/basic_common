package com.mamingchao.basic.unmutable_class;

/**
 * 除了final 类，在就是这种 私有或者 包级私有（子类需在另外的包）的 构造器的类，子类继承类，也没办法 super 父亲类的 构造方法
 * 即没办法 扩展 原来的类
 *
 * 只能通过静态工厂方法获取 一旦生成，不可改变的对象
 *
 * 生成不可变的类的两种方式：1、创建一个final类型的类
 * 2、确保 类的构造函数不可被访问
 * Created by mamingchao on 2020/12/31.
 */
//public class SubUnmutableClass extends unmutableClass {
//
//    private SubUnmutableClass(int field1, String field2) {
//        super(field1, field2);
//    }
//}


