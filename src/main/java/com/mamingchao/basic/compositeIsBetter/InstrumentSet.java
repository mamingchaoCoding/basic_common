package com.mamingchao.basic.compositeIsBetter;

import java.util.Collection;
import java.util.Set;

/**
 * 复合（聚合） 优于 继承
 * 需要一个HashSet， 提供 从对象创建以来
 * Created by mamingchao on 2020/12/31.
 */
public class InstrumentSet<E> extends ForwordingSet<E>{

    private int count;

    public InstrumentSet(Set s) {
        super(s);
    }

    @Override
    public boolean add(E e) {
        count ++;
        return super.add(e);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        count += c.size();
        return super.addAll(c);
    }

}
