package com.mamingchao.basic.comparable;

import java.util.Comparator;

/**
 * comparator constructor method
 * Created by mamingchao on 2020/12/23.
 */
public class PhoneNum implements Comparable<PhoneNum>{
    public short areaNum;
    public short prefix;
    public short lineNum;

    public PhoneNum(short areaNum, short prefix, short lineNum) {
        this.areaNum = areaNum;
        this.prefix = prefix;
        this.lineNum = lineNum;
    }


    private final static Comparator<PhoneNum> COMPARATOR = Comparator.comparingInt((PhoneNum pn) -> pn.areaNum)
            .thenComparingInt(pn ->pn.prefix).thenComparingInt(pn -> pn.lineNum);


    //常规写法
//    private final static Comparator<PhoneNum> COMPARATOR = new Comparator<PhoneNum>() {
//        @Override
//        public int compare(PhoneNum o1, PhoneNum o2) {
//            return 0;
//        }
//    }



    @Override
    public int compareTo(PhoneNum pn) {
        return COMPARATOR.compare(this, pn);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PhoneNum)) {
             return false;
        }

        PhoneNum pn = (PhoneNum)o;

        return Short.compare(pn.areaNum,areaNum) ==0 &&
                Short.compare(pn.prefix,prefix) ==0 &&
                Short.compare(pn.lineNum,lineNum) ==0;

    }

    @Override
    public int hashCode(){
        int prime = 31;
        int result = Short.hashCode(areaNum);
        result = prime * result + Short.hashCode(prefix);
        result = prime * result + Short.hashCode(lineNum);
        return result;
    }

    @Override
    public String toString() {
        return "PhoneNum{" +
                "areaNum=" + areaNum +
                ", prefix=" + prefix +
                ", lineNum=" + lineNum +
                '}';
    }

    public static void main(String[] args) {
        PhoneNum pn1 = new PhoneNum(Short.valueOf("150"),Short.valueOf("5050"),Short.valueOf("5555"));
        PhoneNum pn2 = new PhoneNum(Short.valueOf("160"),Short.valueOf("5050"),Short.valueOf("5555"));

        System.out.println(pn1.equals(pn2));
        System.out.println(pn1.hashCode());
        System.out.println(pn2.hashCode());

        System.out.println(pn1.compareTo(pn2));

    }
}
