package com.mamingchao.basic.function_object;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.DoubleConsumer;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

/**
 * Created by mamingchao on 2021/1/12.
 */
public class UtilFunctionInterface {

    public static void main(String[] args) throws IOException {
        Path dictionary = Paths.get(args[0]);
        List<String> arrayList = new ArrayList<>();
        int[] array = new int[10];

        try(Stream<String> words = Files.lines(dictionary)) {
            words.collect(groupingBy(word -> strSort(word))).values().stream()
                    .filter(group -> group.size() > 5).forEach(group -> System.out.println(group));
        }


        DoubleConsumer dConsumer = new DoubleConsumer() {
            @Override
            public void accept(double value) {

            }
        };
    }

    private static String strSort(String word) {
        char[] a = word.toCharArray();
        Arrays.sort(a);
        return a.toString();
    }
}
