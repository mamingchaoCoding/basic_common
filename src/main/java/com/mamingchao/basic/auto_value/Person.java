package com.mamingchao.basic.auto_value;

/**
 * 生成的java文件在 target -> generated-sources里面
 * 报错也不用关心，compile一下就可以生成 带有 equals hashCode toString 方法的 java文件
 *
 * 文件 一定要 AutoValue_xxxx 格式
 * Created by mamingchao on 2020/12/23.
 */
//@AutoValue
//public abstract class Person {
//    public static Person create(String name, int age) {
//        return new AutoValue_Person(name, age);
//    }
//
//    public abstract String name();
//
//    public abstract int age();
//}
