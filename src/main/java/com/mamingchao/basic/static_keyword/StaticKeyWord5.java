package com.mamingchao.basic.static_keyword;

import static com.mamingchao.basic.tryWithResource.TestTryWithResource.*;

/**
 * static 最后的一个用法 static import
 * 这是一种不常用的用法
 *
 * 加载import上，类.*
 * 后面 在用到这个类里面的者静态方法的时候，就可以直接用类
 * 静态方法，也不用 类.methodName()这种方式调用类
 * Created by mamingchao on 2020/12/16.
 */
public class StaticKeyWord5 {

    public static void main(String[] args) throws Exception{
        testMethod();
    }
}
