package com.mamingchao.basic.static_keyword;

/**
 * 用来修饰成员变量，将其变为类的成员，从而实现所有对象对于该成员的共享；
 * static 修饰的成员变量，不在归这个对象管理，而归于 person这个类管理
 * 即 所有Person的对象，都拥有同一个age属性
 *
 * 比如下面这个例子，当 Person 类的属性 age 不是static的时候，p1 和 p2 都是Person类的对象
 * 每个对象的属性值，自己管理，相互不可见
 *
 * 当 Person 类的属性 age 是static的时候，如上所说，归与Person 这个类管理，age对于p1 和 p2 就可见并共用了
 * Created by mamingchao on 2020/12/16.
 */
public class StaticKeyWord1 {

    public static void main(String[] args) {
        Person p1 = new Person("zhangsan",35);
        Person p2 = new Person("lisi",33);

        System.out.println(p1.toString());
        System.out.println(p2.toString());

    }


    private static class Person{
        String name;
        static int age;
        //int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "person{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
