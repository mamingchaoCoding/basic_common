package com.mamingchao.basic.static_keyword;

/**
 * StaticKeyWord3的另一种写法而已
 * Created by mamingchao on 2020/12/16.
 */
public class StaticKeyWord4 {
    static {
        Person p2 = new Person("person 2 has been initialized");
        Person p4 = new Person("person 4 has been initialized");
    }

    public StaticKeyWord4(String content) {
        System.out.println(content);
    }

    public static void main(String[] args) {

        StaticKeyWord3.print();
        System.out.println("****************");
        StaticKeyWord3 p1 = new StaticKeyWord3("初始化");
    }

    Person p1 = new Person("person 1 has been initialized");
    Person p3 = new Person("person 3 has been initialized");

    static void print(){
        System.out.println("static method has been initialized");
    }
}
