package com.mamingchao.basic.ExtendVsInterface.interfaces;

import com.mamingchao.basic.ExtendVsInterface.AudioClip;
import com.mamingchao.basic.ExtendVsInterface.Song;

/**
 * Created by mamingchao on 2020/12/31.
 */
public interface Singer {

    AudioClip sing(Song song);
}
