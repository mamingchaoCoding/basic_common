package com.mamingchao.basic.skeletal_implements;

import java.util.Iterator;

/**
 * Created by mamingchao on 2020/12/31.
 */
public class MyList extends AbstractCollection{
    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean remove() {
        return false;
    }

    @Override
    public boolean add() {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }
}
