package com.mamingchao.basic.skeletal_implements;

/**
 * Created by mamingchao on 2020/12/31.
 */
public interface Collection {
    boolean isEmpty();
    int size();

    boolean remove();
    boolean add();
    default boolean contain(){
        return false;
    }

}
