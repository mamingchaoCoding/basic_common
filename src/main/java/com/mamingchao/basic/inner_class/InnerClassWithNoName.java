package com.mamingchao.basic.inner_class;

/**
 * 匿名类
 * 匿名类是指没有类名的内部类，必须在创建时使用 new 语句来声明类
 *
 * 匿名类有两种实现方式：
 *  继承一个类，重写其方法。
 *  实现一个接口（可以是多个），实现其方法。
 *
 *  匿名类和局部内部类一样，可以访问外部类的所有成员
 * Created by mamingchao on 2020/12/17.
 */
public class InnerClassWithNoName {

    int a = 10;
    static int b = 20;

    public static void myShow() {
        Outer outer = new Outer(){
            public void show() {
                System.out.println("invoke Outer 匿名内部类 show method");
            }
        };
        outer.show();
    }

    public static void myShow1() {
        OuterInterfacle outer = new OuterInterfacle(){
            public void show() {
                System.out.println("invoke OuterInterfacle 匿名内部类 show method");
            }
        };
        outer.show();
    }

    public static void main(String[] args) {
        myShow();
        myShow1();
    }
}


class Outer{
    public void show(){
        System.out.println("invoke outer show method");
    }
    public void show1(){
        System.out.println("invoke outer show method1");
    }
}

interface OuterInterfacle {
    void show();
}