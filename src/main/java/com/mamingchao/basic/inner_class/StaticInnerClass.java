package com.mamingchao.basic.inner_class;

/**
 * 静态内部类
 * 静态内部类是指使用 static 修饰的内部类
 *
 * 在创建静态内部类的实例时，不需要创建外部类的实例。
 *
 * 静态内部类中可以定义静态成员和实例成员
 * 静态内部类可以直接访问外部类的静态成员，如果要访问外部类的实例成员，则需要通过外部类的实例去访问。
 *
 *
 * Created by mamingchao on 2020/12/17.
 */
public class StaticInnerClass {
    private int a = 10;
    public String b = "temp";
    static int c = 20;
    final boolean flag = true;

    public String method1() {
        return "实例方法1";
    }

    public static String method2() {
        return "实例方法2";
    }

    static class InnerClass{
        StaticInnerClass outer = new StaticInnerClass();
        //访问外部类的非静态成员，需要通过外部类的实例对象
        int a1 = outer.a + 10;
        String b1 = outer.b + "inner";
        boolean flag1 = outer.flag;

        //直接访问外部类的静态成员变量
        int c1 = c + 10;


        static int d1 = 30;
        final static  int e1 = 20;

        String temp1 = outer.method1();
        //直接访问外部类的静态成员方法
        String temp2 = method2();
    }

    public static void main(String[] args) {
        InnerClass ic = new StaticInnerClass.InnerClass();
        System.out.println(ic.a1);
        System.out.println(ic.b1);
        System.out.println(ic.c1);
        System.out.println(ic.flag1);
        System.out.println(ic.temp1);
        System.out.println(ic.temp2);
    }
}
